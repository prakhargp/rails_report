# Callbacks

- Callbacks are a common way for you to execute code at specific times in the life cycle of an Active Record object, for instance just before it is created, after it is saved, or after it is destroyed.

- These can be very useful if you’ve got something to execute whenever an object hits one of those lifecycle points, like modifying the user’s email to be lowercase when creating her account.

- Callbacks are a way of saying something like “Hey Active Record, when you’ve finished creating a new User object, give me a call so I can run this method before anything else happens.”

## What is Object life cycle ?

During the normal operation of a Rails application, objects may be created, updated, and destroyed.

Callbacks provide hooks into specific points (either before, after, or sometimes “around”) in the life cycle of an object. Those life cycle moments are:

- **Initialization** – When the object is first built OR whenever it is reloaded from the database and into memory (so any time you find it in a query).

- **Validation** – whenever Rails checks if the object is valid.

- **Saving** – The actual act of saving an already-built object to the database.

- **Creating** – The creation and saving of a new object.

- **Updating** – The updating of an existing object.

- **Finding** – When you’ve searched for the object.

## Using Callbacks

To use the callbacks, you need to register them. You can implement the callbacks as ordinary methods and use a macro-style class method to register them as callbacks:

    class User < ApplicationRecord
        validates :login, :email, presence: true

        before_validation :ensure_login_has_a_value

        private
        def ensure_login_has_a_value
            if login.nil?
                self.login = email unless email.blank?
            end
        end
    end

The macro-style class methods can also receive a block. Consider using this style if the code inside your block is so short that it fits in a single line:

    class User < ApplicationRecord
        validates :login, :email, presence: true

        before_create do
            self.name = login.capitalize if name.blank?
        end
    end

## Specifying Callback Characteristics

Callbacks give you several options for narrowing down or selecting specifically when you want them to run. If you only want to run a callback when a particular controller action calls it, use the :on option, which takes either a single symbol or a full array, e.g. before_create :run_code, :on => [:create, :update].

You can also use conditional logic options :if and :unless to try a method before running callbacks, for instance:

    before_create :run_code, :unless => :method_is_true

    private

    def method_is_true
        true
    end

## Available Callbacks

Here is a list with all the available Active Record callbacks

1. ## Creating an Object

- before_validation
- after_validation
- before_save
- around_save
- before_create
- around_create
- after_create
- after_save
- after_commit / after_rollback

2. ## Updating an Object

- before_validation
- after_validation
- before_save
- around_save
- before_update
- around_update
- after_update
- after_save
- after_commit / after_rollback

3. ## Destroying an Object

- before_destroy
- around_destroy
- after_destroy
- after_commit / after_rollback

4. ## after_initialize and after_find
 
- The after_initialize callback will be called whenever an Active Record object is instantiated, either by directly using new or when a record is loaded from the database.

- The after_find callback will be called whenever Active Record loads a record from the database. after_find is called before after_initialize if both are defined.

        class User < ApplicationRecord
            after_initialize do |user|
                puts "You have initialized an object!"
            end

            after_find do |user|
                puts "You have found an object!"
            end
        end

        irb> User.new
        You have initialized an object!
        => #<User id: nil>

        irb> User.first
        You have found an object!
        You have initialized an object!
        => #<User id: 1>

4. ## after_touch

- The after_touch callback will be called whenever an Active Record object is touched.

        class User < ApplicationRecord
            after_touch do |user|
                puts "You have touched an object"
            end
        end

        irb> u = User.create(name: 'Kuldeep')
        => #<User id: 1, name: "Kuldeep", created_at: "2013-11-25 12:17:49", updated_at: "2013-11-25 12:17:49">

        irb> u.touch
        You have touched an object
        => true


# Running Callbacks :
The following methods trigger callbacks:

- create
- create!
- destroy
- destroy!
- destroy_all
- destroy_by
- save
- save!
- save(validate: false)
- toggle!
- touch
- update_attribute
- update
- update!
- valid?

Additionally, the after_find callback is triggered by the following finder methods:

- all
- first
- find
- find_by
- find_by_*
- find_by_*!
- find_by_sql
- last

# Skipping Callbacks 

Just as with validations, it is also possible to skip callbacks by using the following methods:

- decrement!
- decrement_counter
- delete
- delete_all
- delete_by
- increment!
- increment_counter
- insert
- insert!
- insert_all
- insert_all!
- touch_all
- update_columns
- update_all
- upsert

# Halting Execution

As you start registering new callbacks for your models, they will be queued for execution. This queue will include all your model's validations, the registered callbacks, and the database operation to be executed.

The whole callback chain is wrapped in a transaction. If any callback raises an exception, the execution chain gets halted and a ROLLBACK is issued. To intentionally stop a chain use:

        class Order < ActiveRecord::Base

            before_save :set_eligibility_for_rebate
            before_save :ensure_credit_card_is_on_file

            def set_eligibility_for_rebate
                self.eligibility_for_rebate ||= false
                throw(:abort)
            end

            def ensure_credit_card_is_on_file
                puts "check if credit card is on file"
            end

        end

        Order.create!
        => ActiveRecord::RecordNotSaved: Failed to save the record

## Relational Callbacks

Much like Active Record relations, callbacks can be performed when related objects are changed. A common example of this might be if you destroy a record and have dependent: :destroy enabled. This proceeds to destroy and orphaned data associated with the parent class. What's great is that you can additionally use a callback to perform more operations if necessary.

        class User < ApplicationRecord
            has_many :articles, dependent: :destroy
        end

        class Article < ApplicationRecord
            after_destroy :log_destroy_action

            def log_destroy_action
                puts 'Article destroyed'
            end
        end

        >> user = User.first
        => #<User id: 1>
        >> user.articles.create!
        => #<Article id: 1, user_id: 1>
        >> user.destroy
        Article destroyed
        => #<User id: 1>

Here, a User has many articles. If a user account is deleted, and dependent: :destroy is in place, you can see the after_destroy callback fire.        